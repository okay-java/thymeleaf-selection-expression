package com.okayjava.selection.exps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafSelectionExpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafSelectionExpsApplication.class, args);
	}

}
