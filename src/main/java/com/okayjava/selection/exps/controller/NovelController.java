package com.okayjava.selection.exps.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.okayjava.selection.exps.vo.Novel;

@Controller
public class NovelController {

	@GetMapping("/view-novel")
	public String viewNovelPage(Model model) {
		Novel novel = new Novel("One Indian Girl", "2020-03-31", 280, 550.00, "Fiction");
		model.addAttribute("novel", novel);
		return "view-novel";
	}
	
	@PostMapping("/add-novel-data")
	public String addNovelData(@ModelAttribute Novel novel , Model model) {
		// write logic to add novel to database
		model.addAttribute("novel", new Novel());
		model.addAttribute("message", "Novel added successfully");
		return "add-novel";
	}
}
