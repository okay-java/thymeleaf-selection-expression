package com.okayjava.selection.exps.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.okayjava.selection.exps.vo.Novel;

@Controller
public class IndexController {

	@GetMapping("/")
	public String indexpage() {
		return "index";
	}
	
	
	
	@GetMapping("/add-novel")
	public String addNovelPage(Model model) {
		model.addAttribute("novel", new Novel());
		return "add-novel";
	}
}
